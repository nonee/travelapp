var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

var User = require('../models/user');

router.use(bodyParser.json());

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.find({})
  .then((users)=>{
    res.json(users);
  },(err)=>next(err))
  .catch((err)=>next(err));  
});
router.post('/', function(req, res, next){
  var user = new User({
    username: req.body.username,
    password: req.body.password,
    birthdate: req.body.birthdate,
    contact: req.body.contact,
    guide: req.body.guide,
    admin: false
  });
  user.save()
  .then((result)=>{
    res.json(result);
  }, (err)=>next(err))
  .catch((err)=>next(err));
});

router.get('/:userinfo', function(req, res, next){
  var userinfo = new Buffer(req.params.userinfo, 'base64').toString('ascii').split(':');
  User.findOne(
    {
      username: userinfo[0],
      birthdate: userinfo[1],
      contact: userinfo[2]
    }
  )
  .then((result)=>{
    res.json(result);
  }, (err)=>next(err))
  .catch((err)=>next(err));
});


router.get('/:username/:birthdate/:contact', function(req, res, next){
  User.findOne(
    {
      username: req.params.username,
      birthdate: req.params.birthdate,
      contact: req.params.contact
    }
  )
  .then((result)=>{
    res.json(result);
  }, (err)=>next(err))
  .catch((err)=>next(err));
});

router.delete('/', function(req, res, next){
  User.remove({})
  .then((result)=>{
    res.json(result);
  }, (err)=>next(err))
  .catch((err)=>next(err));
});


module.exports = router;
