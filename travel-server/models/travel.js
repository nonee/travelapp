const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var courseSchema = new Schema({
    location:{
        type: String,
        required: true
    },
    description:{
        type:String,
        required:true
    },
    plandate:{
        type:String,
        required:true
    },
    starttime:{
        type:String,
        required:true
    },
    endtime:{
        type:String,
        required:true
    }
});

var travelSchema = new Schema({
    name:{
        type: String,
        required:true
    },
    startdate:{
        type:String,
        required:true
    },
    enddate:{
        type:String,
        required:true
    },
    region:{
        type:String,
        required:true
    },
    courses: [courseSchema]
},{timestamps: true});