const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    birthdate:{
        type: String,
        required: true
    },
    contact: {
        type: String,
        required: true
    },
    guide: {
        type: Boolean,
        default: false
    },
    admin: {
        type: Boolean,
        default: false
    }
},
    {timestamps: true}
);

var User = mongoose.model("User", userSchema);
module.exports = User;