import React, { Component } from 'react';
import { AppRegistry, Text, View, StatusBar } from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';

import LoginScreen from './components/LoginScreen';
import HomeScreen from './components/HomeScreen';
import LocationScreen from './components/LocationScreen';
import TravelScreen from './components/TravelScreen';
import SettingScreen from './components/SettingScreen';

//Create the navigation
const MainNav = StackNavigator({
  Home:{
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: '홈'
    }
  },
  Travel:{
    screen: TravelScreen,
    navigationOptions: {
      headerTitle: '여행정보'
    }
  },
  Location:{
    screen: LocationScreen,
    navigationOptions: {
      headerTitle: '위치정보'
    }
  },
  Setting:{
    screen: SettingScreen,
    navigationOptions: {
      headerTitle: '환경설정'
    }
  }
}, {
  initialRouteName:  'Home',
  navigationOptions: {
    headerStyle: {
      marginTop: StatusBar.currentHeight
    }  
  }
});

export default RootNavigator = StackNavigator({
  Login:{
    screen: LoginScreen,
    navigationOptions: {
      headerTitle: '로그인'
    }
  },
  Main: { screen: MainNav },
}, {
  headerMode: 'none'
});

export const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Main' })
  ]
});