import React from 'react';
import { View, Text, Button, StatusBar } from 'react-native';

class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            _id: null,
            username: null,
            isReady: false
        }
    }

    render(){
        return (
            <View style={{marginTop: StatusBar.currentHeight}}>
                <Text>HomeScreen</Text>
                <Button
                    onPress={() => navigate('Login')}
                    title="Go to Login"
                />
            </View>
        );                
    }
}

export default HomeScreen;