import React, { Component, } from 'react';
import {
    StyleSheet,
    View,
    KeyboardAvoidingView,
    Text,
    TextInput,
    Button,
    ScrollView,
    StatusBar,
    Image, 
    ToastAndroid} from 'react-native';

import { resetAction } from '../App'
import InputScrollView from 'react-native-input-scroll-view';
import { getUniqueID } from 'react-native-device-info';

const ls = require('react-native-local-storage');
const loading = 1;
const login = 2;
const error = 3;

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id: null,
            stage: loading,
            name: null,
            birthdate: null,
            contact: null,
            err: null
        };
    }
    // loading stage
    // 1. 기기 id 획득
    // 2. 획득한 id로 사용자 정보 조회
    // 3. 등록되지 않은 경우 login stage로
    // 4. 오류가 발생한 경우 error stage로
    // 5. 사용자 정보 획득 시 home screen으로 이동
    // 
    // login state
    // 1. 입력받은 정보와 기기 id 서버 전송
    // 2. 등록되어 있으나 기기 id가 다를 경우 안내 메시지
    // 3. 등록되어 있지 않을 경우 안내 메시지
    // 4. 등록 또는 갱신 성공 시 home screen으로
    // 5. 오류시 에러 메시지
    //
    // error stage
    // 1. 오류메시지 전송
    // 2. 재시도 버튼 터치 시 loading stage 로

    async componentWillMount (){
        console.log("componentWillMount: 1");
        await this.loadUniqueId();
        console.log("componentWillMount: 2");
        this.action_login();
    }

    loadUniqueId() {
        console.log("loadUniqueId: 1");
        this.getUniqueId().then((uniqueId)=>{
            console.log("loadUniqueId: 2");
            this.setState({_id:uniqueId});
        }, (error)=>{
            console.log("loadUniqueId: 3");
            this.setState({err:error, stage: error});
        });
    }

    getUniqueId() {
        return new Promise(function(resolve, reject)    {
            // 저장된 id를 확인하여 id가 없을 경우 새로 생성, 있을 경우 저장된 id를 state._id로 설정
            ls.get('_id').then((data)=>{
                console.log("getUniqueId: " + data);
                if (data)   {
                    resolve(data);
                }
                else    {
                    const uniqueId = require('react-native-unique-id')
                    uniqueId()
                    .then((uniqueId) => {
                        ls.save('_id', uniqueId);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error);
                    })
                }
            });
        });
    }

    getUserById() {
        // 기기 id로 로그인 시도
        fetch('http://192.168.3.51:3000/users/' + this.state._id)
        .then((response) => response.json())
        .then(result=>{
            console.log('getUserById():', result);
            return result;
        });
    }

    getUserByInfo()    {
        var userInfo = this.state.name + ":" + this.state.birthdate + ":" + this.state.contact;
        var data = new Buffer(userInfo).toString('base64');
        fetch('http://192.168.3.51:3000/users/' + data)
        .then((response) => response.json())
        .then(result=>{
            console.log('getUserByInfo():', result);
            return result;
        });
    }

    async action_login()  {
        console.log('action_login()');
        switch (this.state.stage)   {
            case loading:
                // 로딩 상태는 자동로그인 시도
                if (this.state._id) {
                    try {
                        var user = await this.getUserById();
                        console.log('action_login loading: ', user);
                        if (user)   {
                            this.login();
                        }
                        else{
                            this.setState({stage: login});
                        }
                    }
                    catch(err) {
                        console.log(err);
                        this.setState({err:error, stage: error});
                    };
                }
                else {
                    this.loadUniqueId();
                }
                break;
            case login:
                try {
                    var user = await this.getUserByInfo();
                    console.log('action_login login: ', user);
                    if (user)   {
                        this.login();
                    }
                    else{
                        ToastAndroid.show('등록된 사용자가 아닙니다.', ToastAndroid.SHORT);
                    }
                }
                catch(err) {
                    console.log(err);
                    this.setState({err:error, stage: error});
                };
                break;
            case error:
                this.setState({stage: loading});
                break;
        }
    }
    login() {
        console.log(resetAction);
        this.props.navigation.dispatch(resetAction);
    }

    render(){
        const { navigate } = this.props.navigation;

        switch (this.state.stage)   {
            case error:
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/theone.png')}/>
                        <Text>Error</Text>
                        <Text>{this.state.err}</Text>
                        <Button title="재시도" onPress={() => {this.action_login();}} />
                    </View>
                );
            case login:
                return (
                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior="position"                    >
                        <Image resizeMode="contain" style={styles.logo} source={require('../images/theone.png')}/>
                        <View style={styles.loginForm}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{width:60}}>이름</Text>
                                <TextInput style={{width:100}} value={this.state.name} onChangeText={name => this.setState({ name })}/>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{width:60}}>생년월일</Text>
                                <TextInput style={{width:100}} value={this.state.birthdate} onChangeText={birthdate => this.setState({ birthdate })}/>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{width:60}}>전화번호</Text>
                                <TextInput style={{width:100}} value={this.state.contact} onChangeText={contact => this.setState({ contact })}/>
                            </View>                            
                        </View>
                        <View style={styles.loginButton}>
                            <Button title="로그인" onPress={() => {this.action_login()}} />
                        </View>
                    </KeyboardAvoidingView>
                );
            default:
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/theone.png')}/>
                        <Text>Loading</Text>
                    </View>
                );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: StatusBar.currentHeight,
        alignItems:'center'
    },
    logo: {
        height:200,
        padding:50
    },
    loginForm:{
        marginTop:50,
        alignItems: 'center'
    },
    loginButton: {
      marginTop: 50
    },
  });