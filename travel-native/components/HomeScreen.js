import React from 'react';
import { View, Text } from 'react-native';

var Datastore = require('react-native-local-mongodb')
, db = new Datastore({ filename: 'travel-local-db', autoload: true });

class HomeScreen extends React.Component {
    
    render(){
        const { navigate } = this.props.navigation;

        return (
            <View>
                <Text>HomeScreen</Text>
            </View>
        );
    }
}

export default HomeScreen;