import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StackNavigator } from 'react-navigation';

import LoginScreen from './components/LoginScreen';
import HomeScreen from './components/HomeScreen';
import LocationScreen from './components/LocationScreen';
import TravelScreen from './components/TravelScreen';
import SettingScreen from './components/SettingScreen';

export default App = StackNavigator ({
  Login:{
    screen: LoginScreen,
    navigationOptions: {
      headerTitle: '로그인'
    }
  },
  Home:{
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: '홈'
    }
  },
  Travel:{
    screen: TravelScreen,
    navigationOptions: {
      headerTitle: '여행정보'
    }
  },
  Location:{
    screen: LocationScreen,
    navigationOptions: {
      headerTitle: '위치정보'
    }
  },
  Setting:{
    screen: SettingScreen,
    navigationOptions: {
      headerTitle: '환경설정'
    }
  }
}, {
  initialRouteName:  'Home'
});